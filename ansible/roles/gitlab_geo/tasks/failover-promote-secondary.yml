- name: Promote Secondary Site Postgres, Sidekiq, and Gitaly Omnibus nodes
  command: gitlab-ctl geo promote -f
  tags: promote-secondary
  when:
    - ('postgres' in group_names or 'sidekiq' in group_names or 'gitaly' in group_names)
    - (geo_secondary_site_group_name in group_names)

- name: Promote GitLab Rails nodes for Omnibus environments
  command: gitlab-ctl geo promote -f
  tags: promote-secondary
  when:
    - ('gitlab_rails' in group_names)
    - (geo_secondary_site_group_name in group_names)

- name: Promote Secondary Site for Cloud Native Hybrid environments
  block:
    - name: Configure kubeconfig credentials for Geo secondary site
      become: false
      delegate_to: localhost
      run_once: true
      import_tasks: kubeconfig.yml
      vars:
        geo_site_prefix: "{{ geo_secondary_site_prefix }}"
        geo_site_gcp_project: "{{ geo_secondary_site_gcp_project if cloud_provider == 'gcp' else '' }}"
        geo_site_gcp_zone: "{{ geo_secondary_site_gcp_zone if cloud_provider == 'gcp' else '' }}"
        geo_site_aws_region: "{{ geo_secondary_site_aws_region if cloud_provider == 'aws' else '' }}"

    - name: Promote to Primary Site
      become: false
      delegate_to: localhost
      run_once: true
      kubernetes.core.k8s_exec:
        pod: "{{ task_runner_pod }}"
        namespace: "{{ gitlab_charts_release_namespace }}"
        command: gitlab-rake geo:set_secondary_as_primary

    - name: Add GitLab Charts repo
      become: false
      delegate_to: localhost
      kubernetes.core.helm_repository:
        name: "{{ gitlab_charts_repo_name }}"
        repo_url: "{{ gitlab_charts_repo_url }}"
      tags:
        - reconfigure
        - charts

    - name: Update GitLab Charts repo
      become: false
      delegate_to: localhost
      command: "helm repo update {{ gitlab_charts_repo_name }}"
      tags:
        - reconfigure
        - charts

    - name: Get GitLab Charts values
      become: false
      delegate_to: localhost
      run_once: true
      kubernetes.core.helm_info:
        name: gitlab
        release_namespace: "{{ gitlab_charts_release_namespace }}"
      register: gitlab_info

    - name: Update Geo Role
      become: false
      delegate_to: localhost
      run_once: true
      set_fact:
        gitlab_values: "{{ item.value | regex_replace(\"'role': 'secondary'\", \"'role': 'primary'\") }}"
      loop: "{{ lookup('dict', gitlab_info.status) }}"
      when: "'values' in item.key"

    - name: Get GitLab Charts version if App version specified
      block:
        - name: Get all GitLab Charts versions
          command: helm search repo gitlab/gitlab -l -o json
          register: gitlab_charts_versions
          become: false
          delegate_to: localhost
          run_once: true

        - name: Match GitLab Charts version to App version
          set_fact:
            gitlab_charts_version: "{{ (gitlab_charts_versions.stdout | from_json | selectattr('name', 'equalto', 'gitlab/gitlab') | selectattr('app_version', 'equalto', gitlab_version))[0].version }}"
      when:
        - gitlab_charts_version is not defined
        - gitlab_version != ""

    - name: Update GitLab Charts
      become: false
      delegate_to: localhost
      run_once: true
      kubernetes.core.helm:
        name: gitlab
        chart_ref: gitlab/gitlab
        chart_version: "{{ gitlab_charts_version | default(None) }}"
        update_repo_cache: true
        release_namespace: "{{ gitlab_charts_release_namespace }}"
        values: "{{ gitlab_values }}"
  tags: promote-secondary
  when: cloud_native_hybrid_geo

- name: Disable Maintenance Mode
  import_tasks: maintenance-mode.yml
  vars:
    maintenance_mode_state: false
    maintenance_mode_message: "GitLab is undergoing maintenance"
    site_group_name: "{{ geo_secondary_site_group_name }}"
    site_prefix: "{{ geo_secondary_site_prefix }}"
    site_gcp_project: "{{ geo_secondary_site_gcp_project if cloud_provider == 'gcp' else '' }}"
    site_gcp_zone: "{{ geo_secondary_site_gcp_zone if cloud_provider == 'gcp' else '' }}"
    site_aws_region: "{{ geo_secondary_site_aws_region if cloud_provider == 'aws' else '' }}"
  tags:
    - maintenance-mode
    - maintenance-mode-disable
    - promote-secondary
