- name: Copy Secrets - Copy secrets and SSH host keys from primary NFS server to local
  block:
    - name: Copy Secrets - Copy secrets from primary NFS server to local
      fetch: src={{ gitlab_nfs_path }}/gitlab-secrets.json dest={{ local_tmp }} flat=yes
      tags: secrets

    - name: Copy Secrets - Find all SSH host keys in primary NFS server
      ansible.builtin.find:
        paths: "{{ gitlab_nfs_path }}/ssh/"
        patterns: 'ssh_host_*'
      register: ssh_host_keys
      tags:
        - host_keys
        - secondary

    - name: Copy Secrets - Fetch all SSH host keys from primary NFS to local
      fetch:
        src: "{{ item.path }}"
        dest: "{{ local_tmp }}"
        flat: true
      with_items: "{{ ssh_host_keys.files }}"
      tags:
        - host_keys
        - secondary
  tags: primary
  when:
    - (ansible_default_ipv4.address == geo_primary_gitlab_nfs_int_ip)
    - (geo_primary_site_group_name in group_names)

- name: Copy Secrets - Copy container registry key from primary to secondary
  block:
    - name: Copy Secrets - Copy container registry key from primary to local
      fetch: src=/var/opt/gitlab/gitlab-rails/etc/gitlab-registry.key dest={{ local_tmp }} flat=yes
      when:
        - ('gitlab_rails_primary' in group_names)
        - (geo_primary_site_group_name in group_names)

    - name: Copy Secrets - Copy container registry key from local to secondary app servers
      copy:
        src: "{{ local_tmp }}gitlab-registry.key"
        dest: /var/opt/gitlab/gitlab-rails/etc/gitlab-registry.key
        owner: git
        group: git
        mode: 0644
      when:
        - ('gitlab_rails' in group_names or 'sidekiq' in group_names)
        - (geo_secondary_site_group_name in group_names)
  tags: secrets
  when:
    - container_registry_enable

- name: Copy Secrets - Copy secrets from local to secondary nodes
  copy:
    src: "{{ local_tmp }}gitlab-secrets.json"
    dest: /etc/gitlab/gitlab-secrets.json
    owner: root
    group: root
    mode: 0600
  tags:
    - secondary
    - secrets
  when:
    - (geo_secondary_site_group_name in group_names)
    - omnibus_node

- name: Copy Secrets - Copy secrets from local to secondary nfs
  copy:
    src: "{{ local_tmp }}gitlab-secrets.json"
    dest: "{{ gitlab_nfs_path }}/gitlab-secrets.json"
    owner: root
    group: root
    mode: 0600
  tags:
    - secondary
    - secrets
  when:
    - (ansible_default_ipv4.address == geo_secondary_gitlab_nfs_int_ip)
    - (geo_secondary_site_group_name in group_names)

- name: Copy Secrets - Copy SSH host keys to secondary site
  block:
    - name: Copy Secrets - Get SSH host key names
      ansible.builtin.find:
        paths: "{{ gitlab_nfs_path }}/ssh/"
        patterns: 'ssh_host_*'
      register: ssh_host_keys
      tags: delete

    - name: Copy Secrets - Copy SSH host keys from local to secondary rails nodes
      copy:
        src: "{{ local_tmp }}/{{ item.path.split('/')[-1] }}"
        dest: /etc/ssh
      with_items: "{{ ssh_host_keys.files }}"

    - name: Copy Secrets - Copy SSH host keys from local to secondary nfs server
      copy:
        src: "{{ local_tmp }}/{{ item.path.split('/')[-1] }}"
        dest: "{{ gitlab_nfs_path }}/ssh"
      with_items: "{{ ssh_host_keys.files }}"

    - name: Reload SSH service on rails nodes
      service:
        name: "{{ 'sshd' if ansible_facts['os_family'] == 'RedHat' else 'ssh' }}"
        state: reloaded

    - name: Store file paths to local
      set_fact:
        ssh_host_keys: "{{ ssh_host_keys }}"
      delegate_to: localhost
      tags: delete
  tags:
    - secondary
    - host_keys
  when:
    - ('gitlab_rails' in group_names)
    - geo_secondary_site_group_name in group_names
    - not cloud_native_hybrid_geo

- name: Copy Secrets - Reconfigure GitLab
  command: gitlab-ctl reconfigure
  tags: secondary
  when:
    - (geo_secondary_site_group_name in group_names)
    - omnibus_node

- name: Create Geo secrets for Cloud Native Hybrid environments
  block:
    - name: Copy Secrets - Configure kubeconfig credentials for Geo secondary site
      become: false
      delegate_to: localhost
      run_once: true
      import_tasks: kubeconfig.yml
      vars:
        geo_site_prefix: "{{ geo_secondary_site_prefix }}"
        geo_site_gcp_project: "{{ geo_secondary_site_gcp_project if cloud_provider == 'gcp' else '' }}"
        geo_site_gcp_zone: "{{ geo_secondary_site_gcp_zone if cloud_provider == 'gcp' else '' }}"
        geo_site_aws_region: "{{ geo_secondary_site_aws_region if cloud_provider == 'aws' else '' }}"

    - name: Copy Secrets - Configure GitLab Geo secrets for Cloud Native Hybrid environments
      become: false
      delegate_to: localhost
      run_once: true
      k8s:
        state: present
        definition:
          kind: Secret
          type: Opaque
          metadata:
            name: "geo"
            namespace: "{{ gitlab_charts_release_namespace }}"
          stringData:
            postgresql-password: "{{ postgres_password }}"
            geo-postgresql-password: "{{ postgres_password }}"
  when: secondary_cloud_native_hybrid_geo

- name: Copy Secrets - Delete locally stored secrets from tmp folder
  block:
    - name: Copy Secrets - Delete SSH host keys from local tmp
      file:
        path: "{{ local_tmp }}{{ item.path.split('/')[-1] }}"
        state: absent
      with_items: "{{ ssh_host_keys.files }}"
      when: ssh_host_keys.files is defined

    - name: Copy Secrets - Delete gitlab secrets from local tmp
      file:
        path: "{{ local_tmp }}gitlab-secrets.json"
        state: absent
  delegate_to: localhost
  become: false
  tags:
    - secondary
    - secrets
    - host_keys
    - delete
  when:
    - ('gitlab_rails_primary' in group_names)
    - (geo_secondary_site_group_name in group_names)
